variable "internal_nic" {
  type        = string
  description = "Define o nome da interface de rede"
  default     = "internal-nic-ubuntu"
}

variable "public_ip" {
  type        = string
  description = "Define o nome do ip público"
  default     = "public-ip-ubuntu"
}

variable "ubuntu_server" {
  type        = string
  description = "Define o display name da VM"
  default     = "ubuntu-server"
}

variable "admin_user" {
  type        = string
  description = "Define a senha do usuário xcode.suporte"
  default     = "xcodeit.suporte"
}

variable "admin_password" {
  type        = string
  description = "Define a senha do usuário xcode.suporte"
  default     = "h4ck3rSA@191016ROOT!@#2023"
}

variable "infra_devops_name" {
  type        = string
  description = "Armazena o string infra-devops para usar nos data sources"
  default     = "infra-devops"
}

variable "fqdn_domain" {
  type        = string
  description = "Define o domain label"
  default     = "infra-devops-ubuntu"
}


