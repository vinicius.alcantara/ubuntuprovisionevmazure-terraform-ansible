#!/bin/bash
sleep 120 &&
echo "[serversLinux]" > $(pwd)'/ansible/hosts' && echo $EXTERNAL_IP | tail -n1 >> $(pwd)'/ansible/hosts' && 
ansible-playbook -i $(pwd)'/ansible/hosts' $(pwd)'/ansible/updateOS/playbookUpdateOSLinux.yml' && 
ansible-playbook -i $(pwd)'/ansible/hosts' $(pwd)'/ansible/hardeningOS/playbookHardeningOSLinux.yml' &&
ansible-playbook -i $(pwd)'/ansible/hosts' $(pwd)'/ansible/installZabbixAgent/playbookInstallZabbixAgentLinux.yml' &&
ansible-playbook -i $(pwd)'/ansible/hosts' $(pwd)'/ansible/createUsersLinux/playbookCreateUsersLinux.yml' &&
ansible-playbook -i $(pwd)'/ansible/hosts' $(pwd)'/ansible/mountDiskData/playbookMountDiskData.yml' &&
ansible-playbook -i $(pwd)'/ansible/hosts' $(pwd)'/ansible/installLAMP/playbookInstallLAMP.yml'

