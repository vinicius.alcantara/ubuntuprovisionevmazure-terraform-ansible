terraform {
  required_version = ">=0.12"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "infra-devops" {
  name = var.infra_devops_name
}

data "azurerm_subnet" "infra-devops" {
  name                 = var.infra_devops_name
  virtual_network_name = var.infra_devops_name
  resource_group_name  = data.azurerm_resource_group.infra-devops.name
}

resource "azurerm_public_ip" "public-ip" {
  name                = var.public_ip
  resource_group_name = data.azurerm_resource_group.infra-devops.name
  location            = data.azurerm_resource_group.infra-devops.location
  allocation_method   = "Static"
  domain_name_label   = var.fqdn_domain
}

resource "azurerm_network_interface" "internal-nic" {
  name                = var.internal_nic
  resource_group_name = data.azurerm_resource_group.infra-devops.name
  location            = data.azurerm_resource_group.infra-devops.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = data.azurerm_subnet.infra-devops.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.public-ip.id
  }
}

resource "azurerm_linux_virtual_machine" "ubuntu-server" {
  name                            = var.ubuntu_server
  resource_group_name             = data.azurerm_resource_group.infra-devops.name
  location                        = data.azurerm_resource_group.infra-devops.location
  size                            = "Standard_B2s"
  admin_username                  = var.admin_user
  admin_password                  = var.admin_password
  disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.internal-nic.id,
  ]
  provision_vm_agent = true

  #admin_ssh_key {
  #  username   = var.admin_user
  #  public_key = file("~/.ssh/id_rsa.pub")
  #}

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    #storage_account_type = "Premium_LRS" #SSD DISK
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

}

resource "azurerm_managed_disk" "data-disk-managed" {
  name                 = "${var.ubuntu_server}-data-disk1"
  resource_group_name  = data.azurerm_resource_group.infra-devops.name
  location             = data.azurerm_resource_group.infra-devops.location
  storage_account_type = "Standard_LRS" #HDD DISK
  #storage_account_type = "Premium_LRS" #SSD DISK
  create_option = "Empty"
  disk_size_gb  = 10
}

resource "azurerm_virtual_machine_data_disk_attachment" "disk-attachment" {
  managed_disk_id    = azurerm_managed_disk.data-disk-managed.id
  virtual_machine_id = azurerm_linux_virtual_machine.ubuntu-server.id
  lun                = "1"
  caching            = "ReadWrite"
}

resource "null_resource" "exec-playbooks-ansible" {
  provisioner "local-exec" {
    command =  "bash $(pwd)'/playbooks-run.sh'" #&& sed -i '1d' $(pwd)'/ansible/hosts'

    environment = {
      EXTERNAL_IP = azurerm_public_ip.public-ip.ip_address
    }
  }
}

